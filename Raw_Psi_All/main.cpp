#include "main.h"
#include <ctime>
#include "itensor/all.h"
#include <string>
#include <cmath>
#include <complex>

using namespace itensor;

struct Coef{

    std::vector<int> baseState;
    double value;
};

//int max(std::vector)


std::string generatePrefix(int Ns, int N, double U, double V, int MaxOcc){
    char buffer[100];
    std::sprintf(buffer,"Ns%d_N%d_U%.3f_V%.3f_MO%d",Ns,N,U,V,MaxOcc);
    return std::string(buffer);
}

double coef(std::vector<int>& baseState, MPS& psi, Boson& sites){
    auto state = InitState(sites);

    for(int j=1; j<=sites.length(); j++){
        state.set(j,std::to_string(baseState[j-1]));
    }
    return inner(MPS(state),psi);
}

bool compareCoefs(Coef i1, Coef i2) { 
    return (i1.value > i2.value); 
} 

void calculate(Args const& args = Args::global()){
    std::vector<Coef> coefs;

    auto prefix = generatePrefix(args.getInt("Ns"),args.getInt("N"),args.getReal("U"),args.getReal("V"),args.getInt("MaxOcc"));
    Boson sites;
    readFromFile("../DMRG/data/"+prefix+".sites",sites);
    MPS psi(sites);
    readFromFile("../DMRG/data/"+prefix+".mps",psi);

    if(args.getInt("N")==2){
        for(int n1=1;n1<=args.getInt("Ns");n1++){
            for(int n2=n1; n2<=args.getInt("Ns"); n2++){
                std::vector<int> baseState(args.getInt("Ns"), 0);
                baseState[n1-1]+=1;
                baseState[n2-1]+=1;
                if(*max_element(baseState.begin(), baseState.end())>args.getInt("MaxOcc")){
                    continue;
                }
                coefs.push_back({baseState,coef(baseState,psi,sites)});
            }
        } 
    }

    if(args.getInt("N")==3){
        for(int n1=1;n1<=args.getInt("Ns");n1++){
            for(int n2=n1; n2<=args.getInt("Ns"); n2++){
                for(int n3=n2; n3<=args.getInt("Ns"); n3++){
                    std::vector<int> baseState(args.getInt("Ns"), 0);
                    baseState[n1-1]+=1;
                    baseState[n2-1]+=1;
                    baseState[n3-1]+=1;
                    if(*max_element(baseState.begin(), baseState.end())>args.getInt("MaxOcc")){
                        continue;
                    }
                    coefs.push_back({baseState,coef(baseState,psi,sites)});
                }
            }
        } 
    }

    if(args.getInt("N")==4){
        for(int n1=1;n1<=args.getInt("Ns");n1++){
            for(int n2=n1; n2<=args.getInt("Ns"); n2++){
                for(int n3=n2; n3<=args.getInt("Ns"); n3++){
                    for(int n4=n3; n4<=args.getInt("Ns"); n4++){
                        std::vector<int> baseState(args.getInt("Ns"), 0);
                        baseState[n1-1]+=1;
                        baseState[n2-1]+=1;
                        baseState[n3-1]+=1;
                        baseState[n4-1]+=1;
                        if(*max_element(baseState.begin(), baseState.end())>args.getInt("MaxOcc")){
                            continue;
                        }
                        coefs.push_back({baseState,coef(baseState,psi,sites)});
                    }
                }
            }
        } 
    }

    if(args.getInt("N")==5){
        for(int n1=1;n1<=args.getInt("Ns");n1++){
            for(int n2=n1; n2<=args.getInt("Ns"); n2++){
                for(int n3=n2; n3<=args.getInt("Ns"); n3++){
                    for(int n4=n3; n4<=args.getInt("Ns"); n4++){
                        for(int n5=n4; n5<=args.getInt("Ns"); n5++){
                            std::vector<int> baseState(args.getInt("Ns"), 0);
                            baseState[n1-1]+=1;
                            baseState[n2-1]+=1;
                            baseState[n3-1]+=1;
                            baseState[n4-1]+=1;
                            baseState[n5-1]+=1;
                            if(*max_element(baseState.begin(), baseState.end())>args.getInt("MaxOcc")){
                                continue;
                            }
                            coefs.push_back({baseState,coef(baseState,psi,sites)});
                        }                    
                    }
                }
            }
        } 
    }

    std::sort(coefs.begin(), coefs.end(), compareCoefs);

    std::ofstream file("./data/"+prefix+".rawpsi");
    for(auto& coefv : coefs){
        for(int j=0; j<args.getInt("Ns");j++){   
            file << coefv.baseState[j];
        }
        file << " " << coefv.value << "\n";
    }
    file.close();
}




int main(int argc, char *argv[]){
    addArgs(argc,argv);

    addArg("Ns",8);
    addArg("N",2);

    addArg("U",1.0);
    addArg("V",-1.0);

    addArg("ConserveQNs",true);
    addArg("ConserveNb",true);
    addArg("MaxOcc",2);

    calculate();

    return 0;
}

