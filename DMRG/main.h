#ifndef MAIN_H
#define MAIN_H

#include "itensor/all.h"
#include <string>
#include <cmath>
#include <complex>


#include <fstream>
#include <iomanip>
#include <sstream>

using namespace itensor;
using namespace std::complex_literals;

std::vector<std::string> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

MPS genPsi0(Boson &sites,Args const& args = Args::global()){
    auto state = InitState(sites);
    int Ns = sites.length();
    int N = args.getInt("N");

    int holes = Ns/N-1;

    int j = 1;
    for(int n=0;n<N;n++){
        state.set(j,"1"); j++;
        for(int h=0; h<holes; h++){
            state.set(j,"0"); j++;
        }
    }
    return MPS(state);
}

int process_mem_usage(){
    std::ifstream stat_stream("/proc/self/status",std::ios_base::in);
    int out;
    std::string tmp = "";
    while(tmp != "VmSize:"){
       stat_stream >> tmp;
    }
    stat_stream >> out;
    stat_stream.close();
    return out;
}

int process_peak_mem_usage(){
    std::ifstream stat_stream("/proc/self/status",std::ios_base::in);
    int out;
    std::string tmp = "";
    while(tmp != "VmPeak:"){
       stat_stream >> tmp;
    }
    stat_stream >> out;
    stat_stream.close();
    return out;
}



void addArgs(int argc, char *argv[]){
    for(int i=1; i<argc; i++){
        std::string arg = std::string(argv[i]);
        if( arg.find("{")==string::npos){
            std::cout << "ERROR: Wrong argument!" << std::endl;
        }
        std::string name =arg.substr(0,arg.find("{"));
        std::string type =arg.substr(arg.find("{")+1,1);
        std::string value =arg.substr(arg.find("=")+1);

        if( arg.find(",")!=string::npos){
            auto values = split(value,",");
            for(uint i=0; i<values.size(); i++){
                if(type=="i") Args::global().add(name+"_"+std::to_string(i),atoi(values[i].c_str()));
                if((type=="f")||(type=="d")) Args::global().add(name+"_"+std::to_string(i),atof(values[i].c_str()));
                if(type=="s") Args::global().add(name+"_"+std::to_string(i),values[i]);
                if(type=="b") Args::global().add(name+"_"+std::to_string(i),(bool)atoi(values[i].c_str()));
            }
        } else {
            if(type=="i") Args::global().add(name,atoi(value.c_str()));
            if((type=="f")||(type=="d")) Args::global().add(name,atof(value.c_str()));
            if(type=="s") Args::global().add(name,value);
            if(type=="b") Args::global().add(name,(bool)atoi(value.c_str()));
        }

    }
}

int argLength(std::string name){
    int len = 0;
    auto asd = Args::global();
    while(Args::global().defined(name+"_"+std::to_string(len))==true){
        len++;
    }
    return len;
}

void addArg(std::string name, double value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArgV(std::string name, std::vector<double> values){
    if( Args::global().defined(name+"_"+std::to_string(0))==false){
        for(uint i=0; i<values.size(); i++){
            Args::global().add(name+"_"+std::to_string(i),values[i]);
        }
    }
}

void addArg(std::string name, int value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArgV(std::string name, std::vector<int> values){
    if( Args::global().defined(name+"_"+std::to_string(0))==false){
        for(uint i=0; i<values.size(); i++){
            Args::global().add(name+"_"+std::to_string(i),values[i]);
        }
    }
}

void addArg(std::string name, bool value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void addArgS(std::string name, std::string value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}

void printArg(std::string name, std::string value){
    if( Args::global().defined(name)==false){
        Args::global().add(name,value);
    }
}


double calc(const MPO &obs, const MPS &psi){
    return real(innerC(psi,obs,psi));
}

#endif //MAIN_H

