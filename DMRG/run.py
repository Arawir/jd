import os
import sys

def run(N,NsPerN, U,V):
    Ns=N*NsPerN
    maxOcc=N
    os.system('OMP_NUM_THREADS=1 ./job N{i}=%i Ns{i}=%i U{d}=%.3f V{d}=%.3f MaxOcc{i}=%i > out_N%i_Ns%i_U%.3f_V%.3f_MaxOcc%i.txt &'%(N,Ns,U,V,MaxOcc,N,Ns,U,V,MaxOcc))


N = int(sys.argv[1])


run(N=N,NsPerN=4,U=20.0,V=0.0)
run(N=N,NsPerN=4,U=20.0,V=-1.0)
run(N=N,NsPerN=4,U=20.0,V=-2.0)
run(N=N,NsPerN=4,U=20.0,V=-3.0)
run(N=N,NsPerN=4,U=20.0,V=-1.5)
run(N=N,NsPerN=4,U=20.0,V=-1.9)
run(N=N,NsPerN=4,U=20.0,V=-2.5)
run(N=N,NsPerN=4,U=20.0,V=-2.1)



