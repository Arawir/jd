#include "main.h"
#include <ctime>
#include "itensor/all.h"
#include <string>
#include <cmath>
#include <complex>

using namespace itensor;


MPO hamiltonian(const Boson &sites, Args const& args = Args::global()){
    int Ns = sites.length();
    double U = args.getReal("U");
    double V = args.getReal("V");

    auto ampo = AutoMPO(sites);

    // J /////////////////////////////
    for(int j=1; j<=(Ns-1); j++){
        ampo += -1.0,"A",j,"Adag",j+1;
        ampo += -1.0,"Adag",j,"A",j+1;
    }

    // U /////////////////////////////
    if(U*U > 1e-16){
        for(int j=1; j<=Ns; j++){
            ampo += 0.5*U,"N",j,"N",j;
            ampo += -0.5*U,"N",j;
        }
    }

    // V /////////////////////////////
    if(V*V > 1e-16){
        for(int j=1; j<=(Ns-1); j++){
            for(int dj=1; dj<=8;dj++){
                if(j+dj>Ns){
                    continue;
                }   
                ampo += V/double(dj)/double(dj)/double(dj),"N",j,"N",j+dj;
            }
        }
    }

    return toMPO(ampo);
}

std::string generatePrefix(int Ns, int N, double U, double V, int MaxOcc){
    char buffer[100];
    std::sprintf(buffer,"Ns%d_N%d_U%.3f_V%.3f_MO%d",Ns,N,U,V,MaxOcc);
    return std::string(buffer);
}

void calculate(Args const& args = Args::global()){
    Boson sites = Boson(args.getInt("Ns"));
    MPS psiInit = genPsi0(sites);
    MPO H = hamiltonian(sites);
    /////////////////////////////////////////////////////
    auto sweeps = Sweeps(20);
    sweeps.maxdim() = 10,20,40,80,120,160,200,240,280,320;
    sweeps.cutoff() = 1E-5,1E-6,1E-7,1E-8,1E-9,1E-10;
    sweeps.noise() = 1E-6,1E-7,1E-8,0;

    auto sweeps2 = Sweeps(4);
    sweeps2.maxdim() = 800;
    sweeps2.cutoff() = 1E-10;
    sweeps2.noise() = 0;

    auto [en,psi] = dmrg(H,psiInit,sweeps,{"Quiet=",true});
    auto prefix = generatePrefix(args.getInt("Ns"),args.getInt("N"),args.getReal("U"),args.getReal("V"),args.getInt("MaxOcc"));

    
    double energy0 = inner(psi,H,psi);
    double energy = dmrg(psi,H,sweeps2,{"Quiet=",true});

    for(int i=0; i<50; i++){
	if(energy<=energy0){ 
 	       if((energy0-energy)<1e-8){
        	    break;
        	}
	}
        energy0 = energy;
        energy = dmrg(psi,H,sweeps2,{"Quiet=",true});
    }


    writeToFile("./data/"+prefix+".sites",sites);
    writeToFile("./data/"+prefix+".mps",psi);
}




int main(int argc, char *argv[]){
    addArgs(argc,argv);

    addArg("Ns",8);
    addArg("N",2);

    addArg("U",1.0);
    addArg("V",-1.0);

    addArg("ConserveQNs",true);
    addArg("ConserveNb",true);
    addArg("MaxOcc",2);

    calculate();

    return 0;
}

